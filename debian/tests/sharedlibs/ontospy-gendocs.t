use strict;
use warnings;

use Test::More;
use Test::Command::Simple;
use Cwd qw(cwd);
use File::HomeDir::Test;
use File::HomeDir;
use File::Path qw(make_path);
use Test::TempDir::Tiny;

my @CMD = qw/ontospy gendocs/;
my $CMD = join ' ', @CMD;

my $FOAF = 'file://' . cwd . '/ontospy/tests/rdf/foaf.rdf';

ok $ENV{HOME} = File::HomeDir->my_home, 'create fake $HOME';
ok make_path "$ENV{HOME}/.ontospy/models", 'create $HOME/.ontospy/models';

in_tempdir "gendocs sharedlibs" => sub {
	run_ok @CMD, qw(--type 2), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 4), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 5), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 6), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 7), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 8), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 9), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';
};

done_testing;
